package com.company.tasks;

import java.util.*;

public class keyboardRows {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        String[]words=input.nextLine().split(",");
    }

    public String[] findWords(String[] words) {
        List<String> list = new ArrayList<>(Arrays.asList(words));
        String [] firstRow={"q","w","e","r","t","y","u","i","o","p"};
        String [] secondRow={"a","s","d","f","g","h","j","k","l"};
        String [] thirdRow={"z","x","c","v","b","n","m"};
        Set<String> thirdR = new TreeSet<>(Arrays.asList(thirdRow));
        Set<String> secondR = new TreeSet<>(Arrays.asList(secondRow));
        Set<String> firstR = new TreeSet<>(Arrays.asList(firstRow));


        for (String w : words) {
            String[] letters = w.split("");
            String firstLetter = letters[0].toLowerCase();
            if(firstR.contains(firstLetter)){
                if(!isLegit(firstR, letters)){
                    list.remove(w);
                }
            }
            if(secondR.contains(firstLetter)){
                if(!isLegit(secondR, letters)){
                    list.remove(w);
                }
            }
            if(thirdR.contains(firstLetter)){
                if(!isLegit(thirdR, letters)){
                    list.remove(w);
                }
            }
        }
        return list.toArray(new String[0]);
    }

    public static boolean isLegit(Set<String> row, String[] word){
        for(String c : word){
            if(!row.contains(c.toLowerCase())){
                return false;
            }
        }
        return true;
    }
}
