SELECT Department, Employee, Salary
FROM(
 SELECT
d.name as Department,
e.name  as Employee,
salary,
dense_rank() over (PARTITION by d.name order by salary desc) as rankk
FROM employee e
 INNER JOIN department d
ON e.departmentid = d.id ) ds
WHERE rankk <= 3