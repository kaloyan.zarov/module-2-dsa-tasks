package com.company;

import java.util.Scanner;
import java.util.Stack;

public class MinimumAddToMakeParenthesesValid {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        String S=input.nextLine();
        System.out.println(minAddToMakeValid(S));
    }

    public static int minAddToMakeValid(String S) {
        int closingBracketCounter=0;
        char[] in = S.toCharArray();
        Stack<Character> stack = new Stack<>();
        int counter = 0;
        for (char s : in) {
            stack.push(s);

        }
        while (stack.size() > 0) {
            if (stack.peek().equals(')')) {
                stack.pop();
                if(!stack.isEmpty()) {
                    if (stack.peek().equals(')')) {
                        counter++;
                        closingBracketCounter++;

                    } else {
                        stack.pop();
                    }
                }else{
                    counter++;
                }

            } else if(stack.peek().equals('(')){
                if(closingBracketCounter>0){
                    counter--;
                    closingBracketCounter--;

                }else{
                   counter++;
                }

                stack.pop();

            }

        }
        return counter;
    }
}
