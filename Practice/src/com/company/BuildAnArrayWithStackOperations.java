package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BuildAnArrayWithStackOperations {
    public List<String> buildArray(int[] target, int n) {
        List list = new ArrayList();
        int index=0;

        for(int i = 1; i <=n ; i++) {
            if(index>target.length-1) {
                break;
            }
            if(target[index]!=i) {
                list.add("Push");
                list.add("Pop");
            }else {
                index++;
                list.add("Push");
            }
        }
        return list;
    }
}
