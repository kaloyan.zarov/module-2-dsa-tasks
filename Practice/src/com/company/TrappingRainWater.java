package com.company;

public class TrappingRainWater {
    public int trap(int[] height) {
        int wall = 0;
        int previousWall = 0;
        boolean newWallFound = false;
        int positionOfTheWall = 0;
        int waterCounter = 0;
        for (int i = 0; i < height.length; i++) {

            if (height[i] > wall) {
                wall = height[i];
                newWallFound = true;

                if (newWallFound && previousWall != 0) {
                    waterCounter -= ((i - positionOfTheWall) * (wall - previousWall));

                }
                previousWall = wall;
                positionOfTheWall = i;
            }
            if (height[i] < wall) {
                waterCounter += wall - height[i];
            }
            newWallFound = false;
        }
        return waterCounter;

    }
}
