package com.company;

public class BestTimeToBuy {
    public int maxProfit(int[] prices) {
        int min=Integer.MAX_VALUE;
        int max=0;
        int bestProfit=0;
        for (int i = 0; i < prices.length ; i++) {
            if (prices[i]>max){
                max=prices[i];
            }
            if (prices[i]<min){
                min=prices[i];
                max=0;
            }
            bestProfit=Math.max(bestProfit,max-min);
        }


        return bestProfit;
    }
}
