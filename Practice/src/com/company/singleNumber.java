package com.company.tasks;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

class singleNumber {
    public int singleNumber(int[] nums) {
        HashMap<Integer, Integer> numbers = new HashMap<>();
//        String[] nums = input.nextLine().split(",");
        int counter = nums.length - 1;

        while (counter >= 0) {

            if (numbers.containsKey(nums[counter])) {

                numbers.remove(nums[counter]);
                numbers.put(nums[counter], 2);
            } else if (!numbers.containsKey(nums[counter])) {
                numbers.put(nums[counter], 1);
            }
            counter--;
        }

        for (Map.Entry<Integer,Integer> entry : numbers.entrySet()) {
            if (entry.getValue().equals(1)) {
                return entry.getKey();
            }
        }
        return 0;
    }
}